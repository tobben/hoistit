$fn=12*4;

module ytter(){
  difference(){
    union(){
      cylinder(d=18, h=1);
      cylinder(d=6, h=1.2);
    }
    translate([0,0,-1])
      cylinder(d=4.75, h=3);
  }

  rotate_extrude(angle=90)
    translate([13/2+0.7, 0, 0])
      square([1, 8.4]);
}
//ytter();

module kullager(center=false){
  color("purple")
    translate([0,0,0])
    cylinder(d=13, h=7, center=center);
}
//kullager();

module kullager_v(center=false){
  color("purple")
    translate([0,0,0])
    cylinder(d=12, h=4, center=center);
}
//kullager_v();



module ytter_m_plupp(){
  difference(){
    union(){
      cylinder(d=18, h=1);
    }
    translate([0,0,-1])
      cylinder(d=5, h=4);
  }
}
//ytter_m_plupp();


//difference(){
//  cylinder(d=6, h=1);
//  translate([0,0,-1])
//    cylinder(d=4, h=3);
//}

module hold_tak(){
  difference(){
    union(){
      hull(){
        for(k=[0,1]) mirror([0,k,0])
          for(l=[0,1]) mirror([l,0,0])
            translate([-1,13/2+11/2+4, 0])
              cylinder(d=11, h=1.5);
      }
    }
    for(k=[0,1]) mirror([0,k,0])
      translate([0,13/2+11/2+4, 3])
        cylinder(d=11.2, h=4, center=true);
    for(k=[0,1]) mirror([0,k,0])
      translate([0,13/2+11/2+4, 0])
        cylinder(d=4.5, h=4, center=true);
  }
  difference(){
    union(){
      hull(){
        for(k=[1,0]) mirror([0,k,0])
          translate([0,7, 13])
            rotate([0,90,0])
            cylinder(d=5, h=13, center=true);
        translate([-13/2,-19/2,0])
          cube([13, 19, 1]);
      }
      for(k=[0,1]) mirror([0,k,0])
        difference(){
          translate([-13/2,9,1])
            cube([13, 3, 3]);
          translate([0,12,2.5+1.5])
            rotate([0,90,0])
            cylinder(r=2.5, h=14, center=true);
          translate([0,13/2+11/2+4, 1])
            cylinder(d=11.2, h=1.5);


        }

    }
    translate([0,0.3,-0.3])
      hull(){
        translate([0,0,7.8])
          rotate([0,90,0])
          scale([1.01,1.01,7.7/7])
          kullager(center=true);
        translate([0,-10,7.8])
          rotate([0,90,0])
          scale([1.01,1.01,7.7/7])
          kullager(center=true);
        translate([0,1,19])
          rotate([0,90,0])
          scale([1.01,1.01,7.7/7])
          kullager(center=true);
      }
    translate([0,0,9.3])
      rotate([0,90,0])
      cylinder(d=4.2, h=30, center=true);
  }

  for(k=[0,1]) mirror([k,0,0])
    translate([-7/2-1,0,9.3])
      rotate([0,90,0])
      difference(){
        cylinder(d=4.2+1.5, h=1);
        translate([0,0,-1])
          cylinder(d=4.2, h=3);
      }
  for(k=[0,1]) mirror([k,0,0])
    translate([-7.5,0,9.3])
      rotate([0,90,0])
      difference(){
        cylinder(d=7.8+1.5, h=2, $fn=6);
        translate([0,0,-1])
          cylinder(d=7.8, h=4, $fn=6);
      }
}
//hold_tak();

module pullert(){
  difference(){
    hull(){
      for(k=[0,1]) mirror([0,k,0])
        translate([0,33, 0])
          cylinder(d=11, h=1.5);
    }
    for(k=[0,1]) mirror([0,k,0])
      translate([0,33,0])
        cylinder(d=4.5, h=5, center=true);
  }
  for(k=[0,1]) mirror([0,k,0])
    hull(){
      translate([0,-8,0])
        cylinder(h=1, d=10);
      translate([0,-23,15])
        cylinder(h=1, d=10);
    }
}
//pullert();

module hold_ceiling_unit(){
  difference(){
    union(){
      difference(){
        union(){
          hull(){
            for(k=[0,1]) mirror([0,k,0])
              translate([0,13/2+10/2+4, 0])
                cylinder(d=10, h=1.5);
          }
        }
        for(k=[0,1]) mirror([0,k,0])
          translate([0,13/2+11/2+4, 0])
            cylinder(d=3.5, h=4, center=true);
      }
      hull(){
        for(k=[1,0]) mirror([0,k,0])
          translate([0,7, 13])
            rotate([0,90,0])
              cylinder(d=5, h=10, center=true);
        translate([-10/2,-19/2,0])
          cube([10, 19, 1]);
      }
      for(k=[0,1]) mirror([0,k,0])
        difference(){
          translate([-10/2,9,1])
            cube([10, 3, 3]);
          translate([0,12,2.5+1.5])
            rotate([0,90,0])
              cylinder(r=2.5, h=12, center=true);
        }

    }
    translate([0,0,-0.5])
      hull(){
        translate([0,0,7.8])
          rotate([0,90,0])
            scale([1.1,1.1,8/7])
              kullager_v(center=true);
        translate([0,-1,19])
          rotate([0,90,0])
            scale([1.1,1.1,8/7])
              kullager_v(center=true);
        translate([0,1,19])
          rotate([0,90,0])
            scale([1.1,1.1,8/7])
              kullager_v(center=true);
      }
    translate([0,0,9.3])
      rotate([0,90,0])
        cylinder(d=3.2, h=30, center=true);
    for(k=[0,1]) mirror([0,k,0])
      translate([0,-10/2, -1])
        cylinder(d=3, h=30);
    for(k=[0,1]) mirror([k,0,0])
      translate([-6.5,0,9.3])
        rotate([0,90,0])
          cylinder(d=6.30, h=2, $fn=6);
  }

  for(k=[0,1]) mirror([k,0,0])
    translate([-4/2-1,0,9.3])
      rotate([0,90,0])
        difference(){
          cylinder(d=3.2+1.5, h=1);
          translate([0,0,-1])
            cylinder(d=3.2, h=3);
        }
}
hold_ceiling_unit();


//translate([0,0,9.3])
//rotate([0,90,0])
//kullager(center=true);
