# hoistit

The small blocks I use to hoist Hangprinters up to ceilings.

See it in use [here](https://youtu.be/coI8I0QsQFI).
